# Grand Oral

extraits de https://maths-info-lycee.fr/grantoral.html

* Une fractale permet aussi de jouer... Plus précisément, le casse-tête des tours de Hanoï peut se résoudre à l'aide du triangle de Sierpinski
* la conduite montpelliéraine (problème du chauffeur assassin, homicidal chauffeur pour les moteurs de recherche). Un piéton mobile et lent essaie d'échapper à une voiture rapide mais peu mobile. Il semblerait qu'il existe une méthode stochastique efficace, en plus de la méthode déterministe. Cette modélisation est utilisé par l'armée pour les missiles.
Plus généralement, on peut étudier une <a href="https://fr.wikipedia.org/wiki/Courbe_du_chien">courbe de poursuite.</a>
* Comment générer des nombres aléatoires avec un ordinateur ?
* <a href="s://www.youtube.com/watch?v=L1vDkUziBpw">Un paradoxe amusant</a> en lien avec la série harmonique. On peut donc y mettre aussi le logarithme.
* <a href="https://www.youtube.com/watch?v=WbAgfT79t6w">Le théorème des quatre couleurs.</a>
* Musique et informatique : une alliance possible de l’art et de la science ?
* imprimantes 3D et données informatique : le format STL
* <a href="https://fr.wikipedia.org/wiki/Art_g%C3%A9n%C3%A9ratif">Art génératif</a> : voir en introduction l'exercice sur la ligne d'horizon dans le chapitre diviser pour régner. Ainsi que cette <a href="https://www.youtube.com/watch?v=uazPP0ny3XQ">vidéo</a>, qui montre un lien avec la trigonométrie puis les nombres complexes. Projet de niveau variable, de assez facile jusqu'à très compliqué !
