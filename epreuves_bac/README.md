en 2023 7 sujets sont sortis ( métropole, outre-mer, étranger ).

chaque sujet comprend 3 exercices indépendants, qui chacun requièrent une durée d'environ 1h.

## Epreuve Ecrite (sujets et corrigés):

https://pixees.fr/informatiquelycee/term/suj_bac/

## Epreuve Pratique :

les corrections :
https://pixees.fr/informatiquelycee/term/ep/

## ref BO
https://www.education.gouv.fr/bo/22/Hebdo48/MENE2230418N.htm
