## pour toutes questions envoyez moi un mail :-)

damien.varrel(AT)ac-lyon.fr

## programmer en NSI

Quelques conseils pour progresser:

* Avez-vous bien fait un schéma au brouillon pour visualiser le problème posé ?
* Avez-vous essayé de rédiger un algorithme en français, avec vos propres mots, avant de vous lancer dans la programmation sur machine ?
* Avez-vous utilisé des affichages intermédiaires, des print(), pour visualiser au fur et à mesure le contenu des variables ?
* Avez-vous testé le programme avec les propositions de tests donnés dans l'exercice ?
* Avez-vous testé le programme avec de nouveaux tests, différents de ceux proposés ?

## les serveurs
* Web : http://stex.infos.st:20380
