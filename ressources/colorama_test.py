from colorama import Fore, Back, Style
print(Fore.RED + 'some red text')
print(Fore.YELLOW + Back.GREEN + 'and with a green background')
print(Style.RESET_ALL + Fore.LIGHTBLACK_EX + 'many colors')
print(Style.RESET_ALL + 'back to normal now')