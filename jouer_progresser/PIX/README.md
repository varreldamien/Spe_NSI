## PIX c'est quoi ?

* une certification numérique, pour TOUT le monde ( petits et grands...)
* sera inclus à parcours-sup ( est-ce que les écoles s'en serviront ? )
* vous pouvez vous entraîner sur les compétences que vous voulez (16 en tout)
* pour être certifiable : valider minimum 5 compétences niveau 1 !

## S'entraîner
Sur l'**ENT** via le **Médiacentre** ( surtout ne pas se connecter via pix.fr, votre progression serait perdue )

- tutoriel : https://cloud.pix.fr/s/HteZXG7yL4mo5C8?dir=undefined&openfile=32421

* code à saisir pour remonter votre profil : CBMPRY531

## Certification
fin janvier



