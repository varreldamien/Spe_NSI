* le jeu des nains
* un site avec gestion de base de données
* un générateur de labyrinthe
* un correcteur orthographique
* générateur de QR code ; de code barre
* un automate cellulaire : le jeu de la vie
* un jeu du type "démineur" en mode texte ou avec une interface graphique.
* un jeu du type "2048", en mode texte ou avec une interface graphique
* un jeu du type "Pac-man"
* un jeu du type "casse brique"/"pong"
* un jeu du ""snake"
* jeu de rôle
* animation de tris avec pygame
* animation de la dichotomie
* traitement d'images
* un jeu d'échecs
* un programme permettant de reproduire la vision des daltoniens
* un générateur de mots de passes sécurisés
* cryptage et décryptage de données (méthode de César, Vigenère, cryptage RSA, machine Enigma)
* réalisation d'un générateur de QR code ; de code barre
* le photomaton
* logiciel de lexicographie
* jeu du verger


