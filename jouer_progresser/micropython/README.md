# micropython SoC
micropython, c'est python pour les cartes à base de microcontrôleurs (SoC) comme les microbit, esp32 ou esp8266, raspberry rp2xxx

* en quelque ligne on créé un serveur web
* c'est un moyen simple d'aborder les échanges client-serveur web (méthode GET)

quelques cartes avec connectivité WiFi (esp32 esp8266):\
<img src="ESP32-38Pin.svg" height="128px"/>
<img src="WeMos-D1-mini.svg" height="128px"/>
<img src="WeMos-s2-mini.svg" height="128px"/>
<img src="WeMos-D1-R2.svg" height="256px"/>

## utilisation

* L'IDE Thonny est conseillé
* flasher le firmware correspondant à votre carte ( menu options, onglet firmware)
* exemple de programme webserver.py :
  * connexion à une borne wifi
  * instantiation d'un socket TCP avec écoute sur le port 80 (http)
  * réponse aux clients, avec envoi des entêtes et du code html

https://micropython.org/
