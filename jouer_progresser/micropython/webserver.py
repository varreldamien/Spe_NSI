import socket,network,machine,time

led = machine.Pin(2, machine.Pin.OUT) #led bleue sur la carte sortie 2
led.value(1) #led éteinte

#connexion WiFi
ssid = 'Livebox-C806'
station = network.WLAN(network.STA_IF)
station.active(True)
print("connexion au point d'accès ",ssid,end=": ")
station.connect(ssid, 'bienvenuechezbdv')
while station.isconnected() == False:
    print(".",end="")
    time.sleep_ms(500)
print("succès !")
print(station.ifconfig()) #config réseau

#le contenu de la page web
HTML = """<html>
<head>
    <title>Micropython</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        html {
            font-family: monospace;
            display: inline-block;
            margin: 0px auto;
            text-align: center;
        }
        #ledon,#ledoff{width:40px;padding:5px;display: inline-block;cursor: pointer;border-radius:10%;}
        #ledon{background-color:red;}
        #ledoff{background-color:grey;}
        #ledon:hover,#ledoff:hover{background-color:black;color:white;} 
    </style>
</head>
<body>
    <h2>MicroPython Serveur Web</h2>
    <p>%LEDSTATUS%</p>
    <a href="/on" id=ledon>led ON</a>
    <a href="/off" id=ledoff>led OFF</a>
</body>
</html>"""

# création du serveur
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80)) # écoute de toutes les ip sur le port 80
s.listen(5) # 5 clients max

def send_content(conn):
    """envoi de la page web (entêtes + contenu html)"""
    conn.send('HTTP/1.1 200 OK\n')
    conn.send('Content-Type: text/html\n')
    conn.send('Connection: close\n\n')
    if led.value()==1:
        conn.sendall(HTML.replace("%LEDSTATUS%","led OFF"))
    else:
        conn.sendall(HTML.replace("%LEDSTATUS%","led ON"))
    conn.close()

# boucle d'écoute
while True:
    try:
        conn, addr = s.accept()
        conn.settimeout(3.0)
        print("connexion depuis l'adresse %s" % addr[0])
        request = conn.recv(1024)
        conn.settimeout(None)
        request = str(request)
        if request.find('GET / HTTP/1.1')!=-1: #racine du site demandé
            #affiche toutes les entêtes reçues du client
            print('GET Request Content = %s' % request)
            send_content(conn)
        elif request.find('GET /on HTTP/1.1')!=-1:#demande led on
            led.value(0)
            send_content(conn)
        elif request.find('GET /off HTTP/1.1')!=-1:#demande led off
            led.value(1)
            send_content(conn)
        else:
            conn.send('HTTP/1.1 404 Not Found\n') #demande de page inexistante
            conn.send('Content-Type: text/html\n')
            conn.send('Connection: close\n\n')
            conn.close()
    except OSError as e:
        conn.close()
        print('Connection closed')