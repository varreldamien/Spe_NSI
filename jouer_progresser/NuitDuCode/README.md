<img src="https://camo.githubusercontent.com/13a49cf97b9629c8913734e1e98dab039e2e39d61bd0292f0df903862a78f1b0/68747470733a2f2f7777772e6e7569746475636f64652e6e65742f696d672f6e6463323032332d6c6f676f2e706e67" height="128px">  

# Installation-Utilisation :  
2 possibilités:  
* utiliser python-portable sur une clé USB :
  * télécharger python-portable.zip et décompresser sur votre clé USB (lancer Thonny.exe)
  * des exemples pyxel sont présents dans \Lib\site-packages\pyxel\examples
  * dans user-data, vous pouvez lancer pyxel-edit pour editer les images ou sons.

OU sur votre IDE déjà installé
* https://github.com/kitao/pyxel/blob/main/docs/README.fr.md#comment-installer

présentation et exemples ( tutos ) :

* https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/

documentation complète :

* https://github.com/kitao/pyxel/blob/main/docs/README.fr.md
